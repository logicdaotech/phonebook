import React, { Component } from 'react';
import _ from 'lodash';

const url = "http://localhost:3030/phone";
let editor = '';



class PhoneBook extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true,
      isEdit: false,
    };
  }
  AddFormSubmint(e){
    e.preventDefault();
    let mdata = {
      email: e.target.Iemail.value,
      name: e.target.Iname.value,
      PhoneNumber: e.target.Iphone.value
    }
    //console.log(mdata)


     // [name: Iname, email: Iemail,PhoneNumber: Iphone];
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(mdata)
    }).then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error(`POST запрос на добавление записи по ${url} не удался`);
      }
    })
    .then(ret => {
      this.state.data.push(ret);
      let tmp = this.state.data;
      this.setState({ data: tmp })
      console.log(tmp);
      })
    .catch(error => this.setState({ error, isLoading: false }));



  }
  FormSubmintSavePhon(intem, e){
    e.preventDefault();
    // console.log(intem);
    let mdata = {
      email: e.target.Iemail.value,
      name: e.target.Iname.value,
      PhoneNumber: e.target.Iphone.value
    }

    // console.log(mdata);

    fetch(url+'/'+intem._id, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(mdata)
    }).then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error(`POST запрос на добавление записи по ${url} не удался`);
      }
    }).then(ret => {
      let arr = this.state.data;

      var newArr = _.map(arr, function(a) {

        return a._id === ret._id ? ret : a;
      });
      // console.log('ret ', newArr);


      this.setState({ data: newArr , isEdit: false});

      })
    .catch(error => this.setState({ error, isLoading: false }));

    // this.setState({ isEdit: false });
  }

  editPhoneRec(item, e){
    e.preventDefault();
    this.setState({ isEdit: true })
    // console.log(item);

    editor = (
      <form className="form-group" onSubmit={(eventFormEdit) => this.FormSubmintSavePhon(item, eventFormEdit)}>
          <div className="form-group">
            <input defaultValue={item.email} type="email" className="form-control" name="Iemail" />
            <input defaultValue={item.name} type="text" className="form-control" name="Iname" />
            <input defaultValue={item.PhoneNumber} type="text" className="form-control" name="Iphone" />
          </div>
          <button type="submit" className="btn btn-success">Save</button>
          <button onClick={(e) => this.setState({ isEdit: false })} className="btn btn-secondary">Cancel</button>

      </form>
    )
  }
  delPhoneRec(id, e){
    e.preventDefault();

    fetch(url+'/'+id, {method: 'DELETE'})
    .then(response => {
      if (response.ok) {
        let arr = _.filter(this.state.data,(item) =>{
          return item._id !== id;
        } )
        this.setState({ data: arr })
        return response.json();
      } else {
        throw new Error(`DELETE запрос ${url} не удался`);
      }
    })
    .catch(error => this.setState({ error, isLoading: false }));


  }
  componentDidMount() {
     this.setState({ isLoading: true });
      fetch(url)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(`Get запрос ${url} не удался`);
        }
      })
      .then(ret => this.setState({ data: ret.data, isLoading: false }))
      .catch(error => this.setState({ error, isLoading: false }));

    }

  render() {
    const { data, isLoading, error ,isEdit } = this.state;
    if (error) {
      return <p>{error.message}</p>;
    }
    if (isLoading) {
      return <p>Loading ...</p>;
    }
    let PhonList =(
      <div className="conteiner-fluid">
            {data.map(intem =>
              <div className="card" key={intem._id}>

                  <div className="card-body">
                      <p>name <b>{intem.name}</b></p>
                      <p>Phone <b>{intem.PhoneNumber}</b></p>
                      <p>email <b>{intem.email}</b></p>
                      <p>ID = <b>{intem._id}</b></p>
                  </div>
                  <div className="card-footer">
                      <button className="btn btn-success" onClick={(e) => this.editPhoneRec(intem, e)}>Edit</button>
                      <button className="btn btn-danger" onClick={(e) => this.delPhoneRec(intem._id, e)} >Delete</button>
                  </div>
              </div>
            )}
        </div>)

    let mains = (
      <div>
          <div className="sticky-top card">
              <div className="card-header">
                Добавить новую запись в БД
              </div>
              <div className="card-body">
                  <form className="form-inline" onSubmit={(e) => this.AddFormSubmint(e)}>
                      <div className="form-group">
                        <input defaultValue="test@mail.ru" type="email" className="form-control" name="Iemail" placeholder="you Email" />
                        <input defaultValue="Tester Testovich" type="text" className="form-control" name="Iname" placeholder="you Name" />
                        <input defaultValue="+7 666 666 66 66" type="text" className="form-control" name="Iphone" placeholder="you Phone" />
                      </div>
                      <button type="submit" className="btn btn-primary">add UserPhone</button>
                  </form>
              </div>

          </div>

          { PhonList}

      </div>
    );
    if (isEdit) {
      return editor;
    }
    return mains;
  }



}

export default PhoneBook;
