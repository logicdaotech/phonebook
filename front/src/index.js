import React from 'react';
import ReactDOM from 'react-dom';

import PhoneBook from './components/PhoneBook';


ReactDOM.render(<PhoneBook />, document.getElementById('root'));
