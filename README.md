# phonebook

> phone data. feathersjs + reaktjs



1. Скачайте [NodeJS](https://nodejs.org/) и [npm](https://www.npmjs.com/).
2. Установите необходимые библиотеки

    ```
    cd path/to/phonebook; npm install
    ```
3. скомпилируйте фронтенд и перенесите фронтенд в физерс
    ```
    sh ra.sh
    ```
4. после компиляции приложение запустится автоматически. что бы остановить приложение нажмите CTRL+C. заного запустить приложение

    ```
    npm start
    ```

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers generate model                 # Generate a new Model
$ feathers help                           # Show all commands
```

## License

Copyright (c) 2018 daotech

Licensed under the [MIT license](LICENSE).
